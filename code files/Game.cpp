#include "Game.h"
#include <iostream>

std::string Game::canMove(std::string request)
{
	int i = 0, j = 0,src_x_index = (int)(request[0]),src_y_index = (int)(request[1]),dst_x_index = (int)(request[2]),dst_y_index = (int)(request[3]);
	if (!get_board()[src_x_index][src_y_index])
	{
		return "Invalid move - pick a tool first\n";
	}
	if (check_state(request))//if the game state is in chess state
	{
		return "Invalid move - you cant stay in chess state\n";
	}
	if (get_board()[src_x_index][src_y_index]->getColor == get_board()[dst_x_index][dst_y_index]->getColor)// if a tool from the same color is in the destination index
	{
		return "Invalid move\n";
	}
	else
	{
		move(request);
		return ("%c%c moved to %c%c", (char)src_x_index, (char)src_y_index,(char) dst_x_index, (char)dst_y_index);
	}
}


void  Game::move(std::string request)
{
	Tool* temp = get_board()[(int)(request[0])][(int)(request[1])];// keep the tool you want to move in a temp pointer
	get_board()[(int)(request[2])][(int)(request[3])] = temp;//move the tool to the wanted place
	get_board()[(int)(request[0])][(int)(request[1])] = NULL;//initilize the tool last place with null
}


Game::Game(Tool* Rook, Tool* King, Tool* Knight, Tool* Pawn, Tool* Queen, Tool* Bishop, Tool* R_Rook, Tool* K_King, Tool* K_Knight, Tool* P_Pawn, Tool* Q_Queen, Tool* B_Bishop)//the objects with the big letters before their name are the black tools
{
	int size = 64, i = 0, j = 0;
	this->_board;
	this->_turn = 0;
	_board[0][0] = Rook, _board[0][1] = Knight, _board[0][2] = Bishop, _board[0][3] = King, _board[0][4] = Queen, _board[0][5] = Bishop, _board[0][6] = Knight, _board[0][7] = Rook;//initilizing the first line
	for (i = 0; i < 8; i++)//initilizing the second line
	{
		_board[1][i] = Pawn;
	}
	for (i = 2; i < 6; i++)//initilizing the third untill the sixth line
	{
		for (j = 0; j < 8; i++)
		{
			_board[i][j] = NULL;
		}
	}
	for (i = 0; i < 8; i++)//initilizing the seventh line
	{
		_board[6][i] = Pawn;
	}
	_board[7][0] = Rook, _board[7][1] = Knight, _board[7][2] = Bishop, _board[7][3] = King, _board[7][4] = Queen, _board[7][5] = Bishop, _board[7][6] = Knight, _board[7][7] = Rook;// initilizing the eighth
}


Game::~Game()
{

}


void Game::setTurn(int turn)
{
	this->_turn = turn;
}


Tool***  Game::get_board()
{
	return _board;
}

bool Game::check_state(std::string request)// the func checks if the game is in chess state
{

}