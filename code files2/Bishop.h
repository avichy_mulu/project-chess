#include "Tool.h"
#include <iostream>
class Bishop: public Tool 
{
public:
	Bishop();
	~Bishop();
	virtual int getColor();
	virtual void setColor(int color);
	virtual char getType();
	virtual int traitCheck();
private:
};