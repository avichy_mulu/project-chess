#include "Tool.h"
#include <iostream>
class King: public Tool
{
public:
	King();
	~King();
	virtual int getColor();
	virtual void setColor(int color);
	virtual char getType();
	virtual int traitCheck();
private:
};