#include "Tool.h"
#include <iostream>
class Rook : public Tool 
{
	public:
		Rook();
		~Rook();
		virtual int getColor();
		virtual void setColor(int color);
		virtual char getType();
		virtual int traitCheck();
	private:
};