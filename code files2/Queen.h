#include "Tool.h"
#include <iostream>
class Queen : public Tool
{
public:
	Queen();
	~Queen();
	virtual int getColor();
	virtual void setColor(int color);
	virtual char getType();
	virtual int traitCheck();
private:
};