#pragma once
#include "Tool.h"
#include <iostream>
class Game
{
public:
	bool check_state(int,int, int, int);
	std::string canMove(std::string request);
	void move(std::string request);
	Game(Tool* Rook, Tool* King, Tool* Knight, Tool* Pawn, Tool* Queen, Tool* Bishop, Tool* R_Rook, Tool* K_King, Tool* K_Knight, Tool* P_Pawn, Tool* Q_Queen, Tool* B_Bishop);
	~Game();
	void setTurn(int turn);
	Tool*** get_board();
private:
	Tool* _board[8][8];
	int _turn;
};