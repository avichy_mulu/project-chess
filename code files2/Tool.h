#pragma once
#include <iostream>
class Tool
{
private:
	int _color;
	char _type;
public:
	Tool();
	~Tool();
	virtual int getColor() = 0;
	virtual void setColor(int color)=0;
	virtual char getType() = 0;
	virtual bool traitCheck(int dst_x_index, int dst_y_index) = 0;
};