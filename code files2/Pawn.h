#include "Tool.h"
#include <iostream>
class Pawn : public Tool
{
public:
	Pawn();
	~Pawn();
	virtual int getColor();
	virtual void setColor(int color);
	virtual char getType();
	virtual int traitCheck();
private:
};