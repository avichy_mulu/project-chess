#include "Tool.h"
#include <iostream>
class Knight : public Tool 
{
public:
	Knight();
	~Knight();
	virtual int getColor();
	virtual void setColor(int color);
	virtual char getType();
	virtual int traitCheck();
private:
};